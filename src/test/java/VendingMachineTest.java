import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class VendingMachineTest {
    private final long id = 117345294655382L;
    private int price1 = 8;
    private int price2 = 5;
    private int max1 = 30;
    private int max2 = 40;
    private static VendingMachine vm;
    @BeforeEach
    void setUp() {
        vm = new VendingMachine();
        vm.exitAdminMode();
    }
    @org.junit.jupiter.api.Test
    void getNumberOfProducts() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        assertEquals(30, vm.getNumberOfProduct1());
        assertEquals(40, vm.getNumberOfProduct2());
    }

    @org.junit.jupiter.api.Test
    void getCurrentBalanceTest() {
        assertEquals(0, vm.getCurrentBalance());
    }

    @org.junit.jupiter.api.Test
    void getCurrentModeTest() {
        assertEquals(VendingMachine.Mode.OPERATION, vm.getCurrentMode());
    }

    @org.junit.jupiter.api.Test
    void getCurrentSumOperationModeTest() {
        vm.enterAdminMode(id);
        vm.fillCoins(10, 20);
        vm.exitAdminMode();
        assertEquals(0, vm.getCurrentSum());
    }

    @org.junit.jupiter.api.Test
    void getCurrentSumAdmModeTest() {
        vm.enterAdminMode(id);
        vm.fillCoins(10, 20);
        assertEquals(50, vm.getCurrentSum());
    }

    @org.junit.jupiter.api.Test
    void getCoinsOperationMode() {
        vm.enterAdminMode(id);
        vm.fillCoins(10, 20);
        vm.exitAdminMode();
        assertEquals(0, vm.getCoins1());
        assertEquals(0, vm.getCoins2());

    }

    @org.junit.jupiter.api.Test
    void getCoinsAdmMode() {
        vm.enterAdminMode(id);
        vm.fillCoins(10, 20);
        assertEquals(10, vm.getCoins1());
        assertEquals(20, vm.getCoins2());
    }

    @org.junit.jupiter.api.Test
    void getPrice1() {
        assertEquals(price1, vm.getPrice1());
    }

    @org.junit.jupiter.api.Test
    void getPrice2() {
        assertEquals(price2, vm.getPrice2());
    }


    @org.junit.jupiter.api.Test
    void fillProductsOperationMode() {
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.fillProducts());
    }

    @org.junit.jupiter.api.Test
    void fillProductsAdmMode() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.OK, vm.fillProducts());
    }
    @org.junit.jupiter.api.Test
    void fillCoinsOperationMode() {
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.fillCoins(10, 10));
    }

    @org.junit.jupiter.api.Test
    void fillCoinsZeroValueTest() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.fillCoins(0, 10));
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.fillCoins(10, 0));
    }

    @org.junit.jupiter.api.Test
    void fillCoinsMoreThanMaxTest() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.fillCoins(51, 30));
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.fillCoins(20, 51));
    }

    @org.junit.jupiter.api.Test
    void enterAdminModeInvalidIdTest() {
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.enterAdminMode(1));
    }

    @org.junit.jupiter.api.Test
    void enterAdminModeNonzeroBalanceTest() {
        vm.putCoin1();
        assertEquals(VendingMachine.Response.CANNOT_PERFORM, vm.enterAdminMode(id));
    }

    @org.junit.jupiter.api.Test
    void enterAdminModeTest() {
        assertEquals(VendingMachine.Response.OK, vm.enterAdminMode(id));
        assertEquals(VendingMachine.Mode.ADMINISTERING, vm.getCurrentMode());
    }

    @org.junit.jupiter.api.Test
    void exitAdminMode() {
        vm.enterAdminMode(id);
        vm.exitAdminMode();
        assertEquals(VendingMachine.Mode.OPERATION, vm.getCurrentMode());
    }

    @org.junit.jupiter.api.Test
    void setPricesOperationModeTest() {
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.setPrices(1, 2));
    }

    @org.junit.jupiter.api.Test
    void setPricesInvalidValuesTest() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.setPrices(0, 2));
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.setPrices(2, 0));
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.setPrices(-1, -1));
    }

    @org.junit.jupiter.api.Test
    void setPricesTest() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.OK, vm.setPrices(2, 2));
    }

    @org.junit.jupiter.api.Test
    void putCoin1AdmModeTest() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.putCoin1());
    }

    @org.junit.jupiter.api.Test
    void putCoin1FilledTest() {
        vm.enterAdminMode(id);
        vm.fillCoins(50, 20);
        vm.exitAdminMode();
        assertEquals(VendingMachine.Response.CANNOT_PERFORM, vm.putCoin1());
    }

    @org.junit.jupiter.api.Test
    void putCoins1Test() {
        assertEquals(VendingMachine.Response.OK, vm.putCoin1());
        vm.putCoin1();
        assertEquals(2, vm.getCurrentBalance());
    }
    @org.junit.jupiter.api.Test
    void putCoin2AdmModeTest() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.putCoin2());
    }

    @org.junit.jupiter.api.Test
    void putCoin2FilledTest() {
        vm.enterAdminMode(id);
        vm.fillCoins(20, 50);
        vm.exitAdminMode();
        assertEquals(VendingMachine.Response.CANNOT_PERFORM, vm.putCoin2());
    }

    @org.junit.jupiter.api.Test
    void putCoins2Test() {
        assertEquals(VendingMachine.Response.OK, vm.putCoin2());
        vm.putCoin2();
        assertEquals(4, vm.getCurrentBalance());
    }

    @org.junit.jupiter.api.Test
    void returnMoneyAdmMode() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.returnMoney());
    }

    @org.junit.jupiter.api.Test
    void returnMoneyZeroTest() {
        assertEquals(VendingMachine.Response.OK, vm.returnMoney());
    }

    @org.junit.jupiter.api.Test
    void returnMoneyTest3() {
        vm.putCoin2();
        vm.putCoin1();
        vm.putCoin1();
        assertEquals(VendingMachine.Response.OK, vm.returnMoney());
    }

    @org.junit.jupiter.api.Test
    void returnMoneyTest4() {
        vm.enterAdminMode(id);
        vm.fillCoins(1, 6);
        vm.exitAdminMode();
        vm.putCoin2();
        vm.returnMoney();
        vm.enterAdminMode(id);
        assertEquals(6, vm.getCoins2());
    }

    @org.junit.jupiter.api.Test
    void returnMoneyTest5() {
        vm.enterAdminMode(id);
        vm.fillCoins(1, 6);
        vm.exitAdminMode();
        vm.putCoin1();
        vm.returnMoney();
        vm.enterAdminMode(id);
        assertEquals(1, vm.getCoins1());
    }

    @org.junit.jupiter.api.Test
    void giveProduct1Test1() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.giveProduct1(3));
        vm.exitAdminMode();
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.giveProduct1(0));
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.giveProduct1(31));
        for (int i = 0; i < 5; i++) {
            vm.putCoin2();
        }
        assertEquals(VendingMachine.Response.INSUFFICIENT_PRODUCT, vm.giveProduct1(30));
    }

    @org.junit.jupiter.api.Test
    void giveProduct1Test2() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        assertEquals(VendingMachine.Response.INSUFFICIENT_MONEY, vm.giveProduct1(3));
    }

    @org.junit.jupiter.api.Test
    void giveProduct1Test3() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 5; i++) {
            vm.putCoin2();
        }
        vm.putCoin1();
        assertEquals(VendingMachine.Response.OK, vm.giveProduct1(1));
        vm.enterAdminMode(id);
        assertEquals(4, vm.getCoins2());
        assertEquals(0, vm.getCoins1());
        vm.exitAdminMode();
    }

    @org.junit.jupiter.api.Test
    void giveProduct1Test4() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 5; i++) {
            vm.putCoin2();
        }
        assertEquals(VendingMachine.Response.OK, vm.giveProduct1(1));
        vm.enterAdminMode(id);
        assertEquals(4, vm.getCoins2());
        vm.exitAdminMode();
    }

    @org.junit.jupiter.api.Test
    void giveProduct1Test5() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 20; i++) {
            vm.putCoin1();
        }
        assertEquals(VendingMachine.Response.OK, vm.giveProduct1(1));
        vm.enterAdminMode(id);
        assertEquals(8, vm.getCoins1());
        vm.exitAdminMode();
    }

    @org.junit.jupiter.api.Test
    void giveProduct2Test1() {
        vm.enterAdminMode(id);
        assertEquals(VendingMachine.Response.ILLEGAL_OPERATION, vm.giveProduct2(3));
        vm.exitAdminMode();
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.giveProduct2(0));
        assertEquals(VendingMachine.Response.INVALID_PARAM, vm.giveProduct2(41));
        for (int i = 0; i < 5; i++) {
            vm.putCoin2();
        }
        assertEquals(VendingMachine.Response.INSUFFICIENT_PRODUCT, vm.giveProduct2(40));
    }

    @org.junit.jupiter.api.Test
    void giveProduct2Test2() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        assertEquals(VendingMachine.Response.INSUFFICIENT_MONEY, vm.giveProduct2(3));
    }

    @org.junit.jupiter.api.Test
    void giveProduct2Test3() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 5; i++) {
            vm.putCoin2();
        }
        vm.putCoin1();
        assertEquals(VendingMachine.Response.OK, vm.giveProduct2(1));
        vm.enterAdminMode(id);
        assertEquals(2, vm.getCoins2());
        vm.exitAdminMode();
    }

    @org.junit.jupiter.api.Test
    void giveProduct2Test4() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 5; i++) {
            vm.putCoin2();
        }
        assertEquals(VendingMachine.Response.UNSUITABLE_CHANGE, vm.giveProduct2(1));
    }

    @org.junit.jupiter.api.Test
    void giveProduct2Test5() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 20; i++) {
            vm.putCoin1();
        }
        assertEquals(VendingMachine.Response.OK, vm.giveProduct2(2));
        vm.enterAdminMode(id);
        assertEquals(10, vm.getCoins1());
        vm.exitAdminMode();
    }

    @org.junit.jupiter.api.Test
    void giveProduct2Test6() {
        vm.enterAdminMode(id);
        vm.fillProducts();
        vm.exitAdminMode();
        for (int i = 0; i < 4; i++) {
            vm.putCoin2();
        }
        vm.putCoin1();
        vm.putCoin1();
        assertEquals(VendingMachine.Response.OK, vm.giveProduct2(1));
        vm.enterAdminMode(id);
        assertEquals(0, vm.getCoins1());
        assertEquals(3, vm.getCoins2());
        vm.exitAdminMode();
    }


}